﻿using System.Collections.ObjectModel;

namespace TestParallax
{
    public class ViewModel
    {
        public ViewModel()
        {
            Collection = new ObservableCollection<string>();

            for (var i=0; i < 100; i++)
                Collection.Add(string.Format("Item {0}", i));
        }

        public ObservableCollection<string> Collection { get; set; }
    }
}
