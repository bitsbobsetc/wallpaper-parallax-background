﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace TestParallax
{
    /// <summary>
    /// This Behaviour class is used with the ParallaxBackgroundView control instead of just using Dependency Properties
    /// on the control itself because we need to attach event handler to the instance of the control rather than static
    /// properties
    /// </summary>
    public class ParallaxBackgroundBehaviour
    {
        public static readonly DependencyProperty AttachedGridViewProperty
            = DependencyProperty.RegisterAttached(
                "AttachedGridView",
                typeof(GridView),
                typeof(ParallaxBackgroundBehaviour),
                new PropertyMetadata(null, OnGridViewChanged));

        public static GridView GetAttachedGridView(DependencyObject obj)
        {
            return obj.GetValue(AttachedGridViewProperty) as GridView;
        }

        public static void SetAttachedGridView(DependencyObject obj, GridView gridView)
        {
            obj.SetValue(AttachedGridViewProperty, gridView);
        }

        private static void OnGridViewChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var control = obj as ParallaxBackgroundView;
            if (control == null)
                return;

            if (args.NewValue is GridView)
            {
                control.AttachedGridView = args.NewValue as GridView;
            }
        }
    }
}
