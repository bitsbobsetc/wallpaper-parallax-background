﻿using System;
using System.Linq;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace TestParallax
{
    public sealed partial class ParallaxBackgroundView
    {
        /// <summary>
        /// If a there is a margin to be used between copies of the repeating background then it needs to be accounted for
        /// </summary>
        private const int WallpaperHorizontalMargin = 0;

        private static readonly Uri DefaultImageUri = new Uri("ms-appx:///Assets/RepeatingBackground.png");
        private static ImageSource _defaultImageSource;
        private static int _decodePixelWidth;

        private ScrollViewer _landScapeScrollViewer;

        public ParallaxBackgroundView()
        {
            InitializeComponent();

            Loaded += OnLoaded;
        }

        public static readonly DependencyProperty IsWrapGridProperty =
            DependencyProperty.Register("IsWrapGrid", typeof (bool), typeof (ParallaxBackgroundView),
                                        new PropertyMetadata(false));

        public bool IsWrapGrid
        {
            get { return (bool) GetValue(IsWrapGridProperty); }
            set { SetValue(IsWrapGridProperty, value); }
        }

        private async void OnLoaded(object sender, RoutedEventArgs e)
        {
            //Load the image and determine the width of the image
            var file = await StorageFile.GetFileFromApplicationUriAsync(DefaultImageUri);
            var fileProperties = await file.Properties.GetImagePropertiesAsync();

            _decodePixelWidth = (int) fileProperties.Width;
            _defaultImageSource = new BitmapImage(DefaultImageUri) { DecodePixelWidth = _decodePixelWidth };

            var windowWidth = Window.Current.Bounds.Width;
            var currentWidth = 0.0;

            //Add enough copies of the background image so that there is an extra copies offscreen for scrolling
            while (currentWidth < (windowWidth + _decodePixelWidth))
            {
                var img = new Image
                              {
                                  Source = _defaultImageSource,
                                  Margin = new Thickness(WallpaperHorizontalMargin, 0, 0, 0),
                                  Stretch = Stretch.None
                              };

                BackgroundImageContainer.Children.Add(img);
                currentWidth += _decodePixelWidth;
            }
        }

        /// <summary>
        /// This GridView is set from the ParallaxBackgroundBehaviour class as we can't having it as a dependency property here
        /// would require everything to be static
        /// </summary>
        private GridView _attachedGridView;
        public GridView AttachedGridView
        {
            get { return _attachedGridView; }
            set
            {
                if (_attachedGridView == value)
                    return;

                _attachedGridView = value;

                UpdatedGridView();
            }
        }

        /// <summary>
        /// When the grid view is set then add an event handler for the ViewChanged event of the ScrollViewer inside the GridView
        /// </summary>
        private void UpdatedGridView()
        {
            var scrollViewers = AttachedGridView.GetDescendantsOfType<ScrollViewer>().ToList();
            if (scrollViewers.Count <= 0)
                return;

            var landscapeScrollviewer = scrollViewers[0];

            _landScapeScrollViewer = landscapeScrollviewer;
            landscapeScrollviewer.ViewChanged += ScrollviewerViewChanged;
        }

        /// <summary>
        /// The offset for the repeating background should be a smaller percentage than than the scrollviewer offset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScrollviewerViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            //The offset value here seems to depend on the ItemsPanelTemplate used in the GridView. If using a WrapGrid
            //I had to multiply the offset value by 100.
            var landscapeScrollViewerOffset = _landScapeScrollViewer.HorizontalOffset;

            var delta = (landscapeScrollViewerOffset * 0.3) % ( _decodePixelWidth + WallpaperHorizontalMargin);
            
            BackgroundImageContainer.Margin = new Thickness(-delta, 0, 0, 0);
        }
    }
}
